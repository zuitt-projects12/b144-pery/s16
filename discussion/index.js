/*Repetition Control Structures*/
/*Loops - lets us execute code repeatedly in a pre-set number of time or maybe forever (infinite loop).*/

/*While loop*/
/*It takes in an expression / condition before proceeding in the evaluation of the codes.*/
/*
	Syntax:
	while(expression/condition){
		statement/s;
	}
*/

let count = 5;
while(count !== 0){
	console.log(`While loop: ${count}`);
	count--;
}

/*
	count
	5 4 3 2 1 0
*/

count = 1;
while(count <= 10){
	console.log(`While loop: ${count}`);
	count++;
}

/*Do-while loop*/
/*At least one code block will be executed before proceeding to the condition.*/
/*
	Syntax:
	do{
		statement/s;
	} while(expression/condition)
*/

count = 5;
do{
	console.log(`Do-while loop: ${count}`);
	count--;
} while(count > 0)

count = 1;
do{
	console.log(`Do-while loop: ${count}`);
	count++;
} while(count < 11)

/*For loop - more flexible looping construct */
/*
	Syntax:
	for(initialization; expression/condition; finalExpression){
		statement/s;
	}
*/

for(count = 5; count > 0; count--){
	console.log(`For loop: ${count}`);
}

/*let number = Number(prompt("Give me a number"));

for(let numCount = 1; numCount <= number; numCount++){
	console.log("Hello Batch 144");
}*/

let myString = "alex";
console.log(myString.length);
console.log(myString[2]);

for(let x = 0; x < myString.length; x++){
	console.log(myString[x]);
}

/*Array*/
/*element - represents the values in the array. */
/*index - location of values in the array which starts at index 0.*/
// elements - a l e x
// index    - 0 1 2 3

let myName = "Alex";

for(let i = 0; i < myName.length; i++){
	if(
		myName[i].toLowerCase() == "a" || 
		myName[i].toLowerCase() == "e" || 
		myName[i].toLowerCase() == "i" || 
		myName[i].toLowerCase() == "o" || 
		myName[i].toLowerCase() == "u"){
		// If the letter in the name is a vowel, it will print 3. 
		console.log(3);
	}
	else{
		// Will printg in the console if character is a non-vowel.
		console.log(myName[i]);
	}
}

/*
	continue and break statements

	continue - allows the code to go to the next iteration of the loop without finishing the execution of all statements in a code block.

	break - used to terminate the current loop once a match has been found.
*/

for(count = 0; count <=20; count++){
	// if remainder is equal to 0
	if(count % 2 === 0){
		// tells the code to continue to the next iteration of the loop.
		continue;
	}
	// The current value of number is printed out if the remainder is not equal to 0.
	console.log(`Continue and Break: ${count}`);

	// If the current value of count is greater than 10
	if(count > 10){
		// tells the code to terminate the loop even if the loop condition is still being satisfied.
		break;
	}
}

let name = "Alexandro";

for(i = 0; i < name.length; i++){
	console.log(name[i]);
	if(name[i].toLowerCase() === "a"){
		console.log("Continue to the next iteration");
		continue;
	}

	if(name[i].toLowerCase() === "d"){
		console.log("This will terminate the loop.");
		break;
	}
}