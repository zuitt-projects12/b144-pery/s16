function mainProg(){
	let num = parseInt(prompt("Enter a number"));
	if(!isNaN(num)){
		for(let i = num; i >= 0; i--){
			if(i < 50){
				console.log("This will terminate the loop");
				break;
			}
			else if(i % 10 === 0){
				console.log("The number is divisible by 10. Skipping the number.");
				continue;
			}
			else if(i % 5 === 0){
				console.log(i);
			}
		}
	}
	else{
		alert("Please enter a number!");
		mainProg();
	}
}

mainProg();

let superString = "supercalifragilisticexpialidocious";
let newString = "";

console.log(superString);
for(i = 0; i < superString.length; i++){
	if(superString[i].toLowerCase() === "a" || superString[i].toLowerCase() === "e" ||superString[i].toLowerCase() === "i" || superString[i].toLowerCase() === "o" || superString[i].toLowerCase() === "u"){
		continue;
	}
	else{
		newString += superString[i];
	}
	
}

console.log(newString);